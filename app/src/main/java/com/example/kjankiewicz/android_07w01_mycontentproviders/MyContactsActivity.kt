package com.example.kjankiewicz.android_07w01_mycontentproviders

import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.widget.SimpleCursorAdapter
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_my_contacts.*

class MyContactsActivity : AppCompatActivity() {
    private var mCursor: Cursor? = null

    private fun showContacts() =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (checkSelfPermission("android.permission.READ_CONTACTS") != PackageManager.PERMISSION_GRANTED || checkSelfPermission("android.permission.WRITE_CONTACTS") != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(arrayOf("android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS"),
                        PERMISSIONS_REQUEST_CONTACT_ACCESS)
            } else {
                val mProjection = arrayOf(ContactsContract.Data._ID, ContactsContract.Data.RAW_CONTACT_ID, ContactsContract.Data.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE)

                val mSelection = ContactsContract.Data.MIMETYPE +
                        " = '" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'"

                mCursor = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        mProjection,
                        mSelection,
                        null, null)

                textViewMyContact.text = String.format("%s %s",
                        resources.getText(R.string.no_phones_found).toString(),
                        mCursor!!.count.toString())

                val contactListColumn = arrayOf(ContactsContract.Data.DISPLAY_NAME, ContactsContract.Data.RAW_CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE)

                val contactListItems = intArrayOf(R.id.textViewContactDisplayName, R.id.textViewContactId, R.id.textViewPhoneNumber, R.id.textViewPhoneType)

                val mCursorAdapter = SimpleCursorAdapter(
                        applicationContext,
                        R.layout.my_contact_row,
                        mCursor,
                        contactListColumn,
                        contactListItems,
                        0)

                listContactsView.adapter = mCursorAdapter
            }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_CONTACT_ACCESS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                showContacts()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your contacts cannot be displayed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_contacts)

        showContacts()
    }

    override fun onDestroy() {
        if (!mCursor!!.isClosed) mCursor!!.close()
        super.onDestroy()
    }

    companion object {

        internal var PERMISSIONS_REQUEST_CONTACT_ACCESS = 1
    }
}
