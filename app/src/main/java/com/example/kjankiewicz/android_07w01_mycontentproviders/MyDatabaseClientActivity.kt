package com.example.kjankiewicz.android_07w01_mycontentproviders

import android.arch.persistence.room.Room
import android.content.ContentValues
import android.database.Cursor
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_my_database_client.*

class MyDatabaseClientActivity : AppCompatActivity() {

    private lateinit var mDbHelper: PracZespEtatDbHelper
    private var mCursor: Cursor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_database_client)

        mDbHelper = PracZespEtatDbHelper(applicationContext)

        //Room
        val roomDB = Room.databaseBuilder(
                applicationContext,
                PracZespEtatDatabase::class.java, "room-database-pze.db"
        ).build()
        val pracZespEtatDao = roomDB.PracZespEtatDao()

        createNewRecordsButton.setOnClickListener {

            when {
                databaseRadioButton.isChecked -> {
                    val db = mDbHelper.writableDatabase

                    db.delete(PracZespEtatContract.Zespoly.TABLE_NAME,
                            null, null)

                    val values = ContentValues()
                    for (i in 0..5) {
                        values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP,
                                DataSource.idZespTab[i])
                        values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA,
                                DataSource.nazwaTab[i])
                        values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_ADRES,
                                DataSource.adresTab[i])

                        val recId = db.insert(
                                PracZespEtatContract.Zespoly.TABLE_NAME,
                                null, values)

                        Toast.makeText(
                                applicationContext,
                                "Record with id $recId inserted.",
                                Toast.LENGTH_SHORT).show()
                    }
                    db.close()
                }
                contentProviderRadioButton.isChecked -> {

                    contentResolver.delete(PracZespEtatContract.Zespoly.CONTENT_URI, null, null)

                    val values = ContentValues()
                    for (i in 0..5) {
                        values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP, DataSource.idZespTab[i])
                        values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA, DataSource.nazwaTab[i])
                        values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_ADRES, DataSource.adresTab[i])

                        contentResolver.insert(PracZespEtatContract.Zespoly.CONTENT_URI, values)
                    }
                }
                else -> {
                    CreateNewRecords().execute(pracZespEtatDao)
                }
            }
        }

        updateSomeRecordsButton.setOnClickListener {

            val values = ContentValues()
            values.put(PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA, "NEW_WRONG")

            val selection = PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP + " = ?"
            val selectionArgs = arrayOf(60.toString())

            when {
                databaseRadioButton.isChecked -> {
                    val db = mDbHelper.writableDatabase

                    val count = db.update(
                            PracZespEtatContract.Zespoly.TABLE_NAME,
                            values,
                            selection,
                            selectionArgs)

                    val toast = Toast.makeText(
                            applicationContext,
                            "Zmodyfikowano $count rek.",
                            Toast.LENGTH_SHORT)
                    toast.show()

                    db.close()
                }
                contentProviderRadioButton.isChecked -> {
                    val nRows = contentResolver.update(
                            PracZespEtatContract.Zespoly.CONTENT_URI, values, selection, selectionArgs)

                    val toast = Toast.makeText(
                            applicationContext,
                            "Zmodyfikowano $nRows rek.",
                            Toast.LENGTH_SHORT)
                    toast.show()
                }
                // Room
                else -> {
                    UpdateSomeRecords().execute(pracZespEtatDao)
                }
            }
        }

        deleteSomeRecordsButton.setOnClickListener {

            val selection = PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP + " = ?"
            val selectionArgs = arrayOf(60.toString())

            when {
                databaseRadioButton.isChecked -> {
                    val db = mDbHelper.writableDatabase

                    val count = db.delete(
                            PracZespEtatContract.Zespoly.TABLE_NAME, selection, selectionArgs)

                    val toast = Toast.makeText(
                            applicationContext,
                            "Usunięto $count rek.",
                            Toast.LENGTH_SHORT)
                    toast.show()

                    db.close()
                }
                contentProviderRadioButton.isChecked -> {
                    val nRows = contentResolver.delete(
                            PracZespEtatContract.Zespoly.CONTENT_URI, selection, selectionArgs)

                    Toast.makeText(
                            applicationContext,
                            "Usunięto $nRows rek.",
                            Toast.LENGTH_SHORT).show()
                }
                // Room
                else -> {
                    DeleteSomeRecords().execute(pracZespEtatDao)
                }
            }
        }

        getNoRecordsButton.setOnClickListener {

            val projection = arrayOf(PracZespEtatContract.Zespoly._ID,
                    PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP,
                    PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA,
                    PracZespEtatContract.Zespoly.COLUMN_NAME_ADRES)
            val sortOrder = PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA + " DESC"

            when {
                databaseRadioButton.isChecked -> {
                    val db = mDbHelper.readableDatabase

                    mCursor = db.query(
                            PracZespEtatContract.Zespoly.TABLE_NAME,
                            projection, null, null, null, null,
                            sortOrder
                    )

                    val toast = Toast.makeText(
                            applicationContext,
                            "Znaleziono " + mCursor!!.count.toString() + " rek.",
                            Toast.LENGTH_SHORT)
                    toast.show()
                    db.close()
                }
                contentProviderRadioButton.isChecked -> {
                    mCursor = contentResolver.query(
                            PracZespEtatContract.Zespoly.CONTENT_URI,
                            projection, null, null,
                            sortOrder)

                    if (mCursor != null) {
                        Toast.makeText(
                                applicationContext,
                                "Znaleziono " + mCursor!!.count.toString() + " rek.",
                                Toast.LENGTH_SHORT).show()
                    }
                }
                // Room
                else -> {
                    GetNoRecords().execute(pracZespEtatDao)
                }
            }
        }

        refreshListOfRecordsButton.setOnClickListener {
            val projection = arrayOf(PracZespEtatContract.Zespoly._ID, PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP, PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA, PracZespEtatContract.Zespoly.COLUMN_NAME_ADRES)
            val sortOrder = PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA + " DESC"
            val mZespListItems = intArrayOf(R.id.textViewIdZesp, R.id.textViewNazwa, R.id.textViewAdres)
            val mCursorAdapter: ListAdapter

            when {
                databaseRadioButton.isChecked -> {
                    val db = mDbHelper.readableDatabase

                    mCursor = db.query(
                            PracZespEtatContract.Zespoly.TABLE_NAME,
                            projection,
                            null,
                            null,
                            null,
                            null,
                            sortOrder
                    )
                    val mZespColumn = arrayOf(
                            PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP, PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA, PracZespEtatContract.Zespoly.COLUMN_NAME_ADRES)

                    mCursorAdapter = SimpleCursorAdapter(
                            applicationContext,
                            R.layout.my_zesp_row,
                            mCursor,
                            mZespColumn,
                            mZespListItems,
                            0)
                }
                contentProviderRadioButton.isChecked -> {
                    mCursor = contentResolver.query(
                            PracZespEtatContract.Zespoly.CONTENT_URI,
                            projection, null, null,
                            sortOrder)

                    val mZespColumn = arrayOf(
                            PracZespEtatContract.Zespoly.COLUMN_NAME_ID_ZESP, PracZespEtatContract.Zespoly.COLUMN_NAME_NAZWA, PracZespEtatContract.Zespoly.COLUMN_NAME_ADRES)

                    mCursorAdapter = SimpleCursorAdapter(
                            applicationContext,
                            R.layout.my_zesp_row,
                            mCursor,
                            mZespColumn,
                            mZespListItems,
                            0)

                }
                // Room
                else -> {
                    mCursor = GetAllZespoly().execute(pracZespEtatDao).get()

                    val mZespColumn = arrayOf(
                            Zespol::idZesp.name, Zespol::nazwa.name, Zespol::adres.name)

                    mCursorAdapter = SimpleCursorAdapter(
                            applicationContext,
                            R.layout.my_zesp_row,
                            mCursor,
                            mZespColumn,
                            mZespListItems,
                            0)
                }
            }
            zespolyListView.adapter = mCursorAdapter
        }
    }

    override fun onDestroy() {
        if (mCursor?.isClosed == true)
            mCursor!!.close()
        super.onDestroy()
    }


    // primitive AsyncTasks for Room
    private inner class CreateNewRecords : AsyncTask<PracZespEtatDao, Void, Int>() {
        override fun doInBackground(vararg pracZespEtatDao: PracZespEtatDao): Int {
            pracZespEtatDao[0].deleteAllZespoly()
            val zespoly = DataSource.zespoly()
            return pracZespEtatDao[0].insertZespolyAll(*zespoly.toTypedArray()).size
        }

        override fun onPostExecute(result: Int?) {
            Toast.makeText(
                    applicationContext,
                    "Wstawiono $result rek.",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private inner class UpdateSomeRecords : AsyncTask<PracZespEtatDao, Void, Int>() {
        override fun doInBackground(vararg pracZespEtatDao: PracZespEtatDao): Int {
            val zesp = Zespol(60, "NEW_WRONG", "BAD")
            return pracZespEtatDao[0].updateZespol(zesp)
        }

        override fun onPostExecute(result: Int?) {
            Toast.makeText(
                    applicationContext,
                    "Zmodyfikowano $result rek.",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private inner class DeleteSomeRecords : AsyncTask<PracZespEtatDao, Void, Int>() {
        override fun doInBackground(vararg pracZespEtatDao: PracZespEtatDao): Int {
            val zesp = Zespol(60, "", "")
            return pracZespEtatDao[0].deleteZespol(zesp)
        }

        override fun onPostExecute(result: Int?) {
            Toast.makeText(
                    applicationContext,
                    "Usunięto $result rek.",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private inner class GetNoRecords : AsyncTask<PracZespEtatDao, Void, Int>() {
        override fun doInBackground(vararg pracZespEtatDao: PracZespEtatDao): Int {
            return pracZespEtatDao[0].getNoZespoly()
        }

        override fun onPostExecute(result: Int?) {
            Toast.makeText(
                    applicationContext,
                    "Znaleziono $result rek.",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private inner class GetAllZespoly : AsyncTask<PracZespEtatDao, Void, Cursor>() {
        override fun doInBackground(vararg pracZespEtatDao: PracZespEtatDao): Cursor {
            return pracZespEtatDao[0].getAllZespoly()
        }
    }

}
