package com.example.kjankiewicz.android_07w01_mycontentproviders

object DataSource {
    val idZespTab = intArrayOf(10, 20, 30, 40, 50, 60)
    val nazwaTab = arrayOf("ADMINISTRACJA", "SYSTEMY ROZPROSZONE", "SYSTEMY EKSPERCKIE", "ALGORYTMY", "BADANIA OPERACYJNE", "WRONG")
    val adresTab = arrayOf("PIOTROWO 3A", "PIOTROWO 3A", "STRZELECKA 14", "WLODKOWICA 16", "MIELZYNSKIEGO 30", "BAD")

    fun zespoly(): MutableList<Zespol> {
        val zespoly = mutableListOf<Zespol>()
        for (i in 0..5)
            zespoly.add(i, Zespol(idZespTab[i].toLong(), nazwaTab[i], adresTab[i]))
        return zespoly
    }
}