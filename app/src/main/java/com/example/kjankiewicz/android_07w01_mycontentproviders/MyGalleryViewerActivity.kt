package com.example.kjankiewicz.android_07w01_mycontentproviders

import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.SimpleCursorAdapter
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_my_gallery_viewer.*

class MyGalleryViewerActivity : AppCompatActivity() {
    private var mCursor: Cursor? = null

    private fun showPictures() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") !=
                PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf("android.permission.READ_EXTERNAL_STORAGE"),
                    PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
        } else {
            val mProjection = arrayOf(
                    MediaStore.Images.ImageColumns._ID,
                    MediaStore.Images.ImageColumns.DISPLAY_NAME,
                    MediaStore.Images.ImageColumns.SIZE,
                    MediaStore.Images.ImageColumns.DESCRIPTION,
                    MediaStore.Images.ImageColumns.DATA)

            val mSelection =
                    MediaStore.Images.ImageColumns.DISPLAY_NAME +
                            " like ? "

            val mSelectionArgs = arrayOf("%jpg%")

            val mOrderBy =
                    MediaStore.Images.ImageColumns.SIZE + " ASC "

            mCursor = contentResolver.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    mProjection,
                    mSelection,
                    mSelectionArgs,
                    mOrderBy)

            textViewInfo.text = String.format("%s %s",
                    resources.getText(R.string.no_photos_found).toString(),
                    mCursor!!.count.toString())

            val mImagesListColumn = arrayOf(
                    //MediaStore.Images.ImageColumns._ID,
                    MediaStore.Images.ImageColumns.DISPLAY_NAME,
                    MediaStore.Images.ImageColumns.SIZE,
                    MediaStore.Images.ImageColumns.DESCRIPTION,
                    MediaStore.Images.ImageColumns.DATA)

            val mImagesListItems = intArrayOf(
                    R.id.textViewFileName,
                    R.id.textViewFileSize,
                    R.id.textViewDescription,
                    R.id.imageViewImageData)

            val mCursorAdapter = SimpleCursorAdapter(
                    applicationContext,
                    R.layout.my_gallery_row,
                    mCursor,
                    mImagesListColumn,
                    mImagesListItems,
                    0)

            listImagesView.adapter = mCursorAdapter
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showPictures()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your pictures cannot be displayed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_gallery_viewer)

        showPictures()

    }

    override fun onDestroy() {
        if (!mCursor!!.isClosed) mCursor!!.close()
        super.onDestroy()
    }

    companion object {

        internal var PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1
    }
}
