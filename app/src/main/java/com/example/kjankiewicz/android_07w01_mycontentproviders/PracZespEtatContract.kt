package com.example.kjankiewicz.android_07w01_mycontentproviders

import android.net.Uri

open class KBaseColumns  {
    val _ID = "_id"
}

class PracZespEtatContract {

    class Zespoly: KBaseColumns() {
        companion object : KBaseColumns() {
            val CONTENT_URI: Uri = Uri.withAppendedPath(AUTHORITY_URI, "zespoly")
            internal const val TABLE_NAME = "zespoly"
            internal const val COLUMN_NAME_ID_ZESP = "id_zesp"
            internal const val COLUMN_NAME_NAZWA = "nazwa"
            internal const val COLUMN_NAME_ADRES = "adres"
        }
    }

    class Etaty : KBaseColumns() {
        companion object : KBaseColumns() {
            val CONTENT_URI: Uri = Uri.withAppendedPath(AUTHORITY_URI, "etaty")
            internal const val TABLE_NAME = "etaty"
            internal const val COLUMN_NAME_NAZWA = "nazwa"
            internal const val COLUMN_NAME_PLACA_MAX = "placa_min"
            internal const val COLUMN_NAME_PLACA_MIN = "placa_max"
        }
    }

    class Pracownicy : KBaseColumns() {
        companion object : KBaseColumns() {
            val CONTENT_URI: Uri = Uri.withAppendedPath(AUTHORITY_URI, "pracownicy")
            internal const val TABLE_NAME = "pracownicy"
            internal const val COLUMN_NAME_ID_PRAC = "id_prac"
            internal const val COLUMN_NAME_NAZWISKO = "nazwisko"
            internal const val COLUMN_NAME_ETAT = "etat"
            internal const val COLUMN_NAME_PLACA_POD = "placa_pod"
            internal const val COLUMN_NAME_PLACA_DOD = "placa_dod"
            internal const val COLUMN_NAME_ID_SZEFA = "id_szefa"
            internal const val COLUMN_NAME_ID_ZESP = "id_zesp"
        }
    }

    companion object {

        internal const val AUTHORITY = "com.example.kjankiewicz.android_07w01_mycontentproviders.praczespetat"

        internal val AUTHORITY_URI = Uri.parse("content://$AUTHORITY")
    }

}
