package com.example.kjankiewicz.android_07w01_mycontentproviders

import android.arch.persistence.room.*
import android.database.Cursor

@Entity(tableName = "zespoly")
data class Zespol(
        @PrimaryKey(autoGenerate = true) var idZesp: Long?,
        @ColumnInfo(name = "nazwa") var nazwa: String,
        @ColumnInfo(name = "adres") var adres: String?
)

@Entity(tableName = "etaty")
data class Etat(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "nazwa") var nazwa: String,
        @ColumnInfo(name = "placa_min") var placaMin: Float,
        @ColumnInfo(name = "placa_max") var placaMax: Float
)

@Entity(tableName = "pracownicy")
data class Pracownik(
        @PrimaryKey(autoGenerate = true) var idPrac: Long?,
        @ColumnInfo(name = "nazwisko") var nazwisko: String,
        @ColumnInfo(name = "etat") var etat: String,
        @ColumnInfo(name = "placa_pod") var placaPod: Float,
        @ColumnInfo(name = "placa_dod") var placaDod: Float?,
        @ColumnInfo(name = "id_szefa") var idSzefa: Int?,
        @ColumnInfo(name = "id_zesp") var idZesp: Int
)

@Dao
interface PracZespEtatDao {
    @Query("SELECT idZesp as _id, idZesp, nazwa, adres FROM zespoly")
    fun getAllZespoly(): Cursor

    @Query("SELECT count(*) as ile FROM zespoly")
    fun getNoZespoly(): Int

    @Query("SELECT * FROM zespoly WHERE idZesp IN (:zespolyIds)")
    fun loadAllZespolyByIds(zespolyIds: IntArray): List<Zespol>

    @Query("SELECT * FROM zespoly WHERE nazwa LIKE :nazwa LIMIT 1")
    fun findZespolyByNazwa(nazwa: String): Zespol

    @Query("DELETE from zespoly")
    fun deleteAllZespoly()

    @Insert
    fun insertZespolyAll(vararg zespoly: Zespol): Array<Long>

    @Delete
    fun deleteZespol(zespol: Zespol): Int

    @Update
    fun updateZespol(zespol: Zespol): Int
}

@Database(entities = arrayOf(Pracownik::class, Zespol::class, Etat::class), version = 1, exportSchema = false)
abstract class PracZespEtatDatabase : RoomDatabase() {
    abstract fun PracZespEtatDao(): PracZespEtatDao
}


