package com.example.kjankiewicz.android_07w01_mycontentproviders

import android.content.ContentProviderOperation
import android.content.ContentValues
import android.content.Intent
import android.content.OperationApplicationException
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.RemoteException
import android.provider.ContactsContract
import android.provider.ContactsContract.RawContacts
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast

import java.util.ArrayList

import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var mKJRawIndexId: Long = 0

    private fun getRawIdByDisplayName(accountName: String?): Long {

        var rawContactId: Long = 0
        val accountType = ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME
        var selection = RawContacts.DELETED + "=0"
        var selectionArgs: Array<String>? = null
        val contentResolver = contentResolver

        // Only get contacts from the selected account
        if (accountName != null) {
            selection += " AND " + RawContacts.ACCOUNT_NAME + "=? AND " + RawContacts.ACCOUNT_TYPE + "=?"
            selectionArgs = arrayOf(accountName, accountType)
        }

        val contacts = contentResolver.query(RawContacts.CONTENT_URI,
                arrayOf(RawContacts._ID), selection, selectionArgs, null)

        if (contacts != null) {
            val indexRawContactId = contacts.getColumnIndex(RawContacts._ID)
            if (contacts.moveToFirst()) {
                rawContactId = contacts.getInt(indexRawContactId).toLong()
            }
            contacts.close()
        }
        Log.i("getRawIdByDisplayName-A", rawContactId.toString())

        if (rawContactId == 0L) {
            /*ContentValues values = new ContentValues();
            values.put(RawContacts.ACCOUNT_NAME, accountName);
            values.put(RawContacts.ACCOUNT_TYPE, accountType);
            Uri rawContactUri = contentResolver.insert(RawContacts.CONTENT_URI, values);
            rawContactId = ContentUris.parseId(rawContactUri);*/

            val ops = ArrayList<ContentProviderOperation>()
            val rawContactInsertIndex = ops.size

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, accountType)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, accountName)
                    .build())

            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, accountName)
                    .build())
            try {
                val res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops)
                rawContactId = Integer.parseInt(res[1].uri.lastPathSegment!!).toLong()
            } catch (e: RemoteException) {
                e.printStackTrace()
            } catch (e: OperationApplicationException) {
                e.printStackTrace()
            }

        }
        Log.i("getRawIdByDisplayName-B", rawContactId.toString())
        return rawContactId
    }

    private fun activateContactButtons() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (checkSelfPermission("android.permission.READ_CONTACTS") !=
                        PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission("android.permission.WRITE_CONTACTS") !=
                        PackageManager.PERMISSION_GRANTED)) {
            requestPermissions(arrayOf("android.permission.READ_CONTACTS",
                    "android.permission.WRITE_CONTACTS"),
                    PERMISSIONS_REQUEST_CONTACT_ACCESS)
        } else {
            mKJRawIndexId = getRawIdByDisplayName(resources.getString(R.string.contact_to_edit))

            newPhoneButton.setOnClickListener {
                // insert new phoneŁ
                val values = ContentValues()
                values.put(ContactsContract.Data.RAW_CONTACT_ID, mKJRawIndexId)
                values.put(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "1234567890")
                values.put(
                        ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                contentResolver.insert(ContactsContract.Data.CONTENT_URI, values)

            }
            newPhoneButton.isEnabled = true

            updatePhoneButton.setOnClickListener {

                val mSelection = ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " +
                        ContactsContract.Data.MIMETYPE + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.NUMBER + " = ? "

                val mSelectionArgs = arrayOf(
                        mKJRawIndexId.toString(),
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        "1234567890")

                val values = ContentValues()
                values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "DELETE_ME")
                values.putNull(ContactsContract.CommonDataKinds.Phone.TYPE)
                val nRows = contentResolver.update(
                        ContactsContract.Data.CONTENT_URI, values, mSelection, mSelectionArgs)

                Toast.makeText(
                        applicationContext,
                        "Zmodyfikowano $nRows tel.",
                        Toast.LENGTH_SHORT).show()
            }
            updatePhoneButton.isEnabled = true

            deletePhoneButton.setOnClickListener {
                val mSelection = ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.NUMBER + " = ? "

                val mSelectionArgs = arrayOf(mKJRawIndexId.toString(), "DELETE_ME")

                val nRows = contentResolver.delete(
                        ContactsContract.Data.CONTENT_URI, mSelection, mSelectionArgs)

                Toast.makeText(
                        applicationContext,
                        "Usunięto $nRows tel.",
                        Toast.LENGTH_SHORT).show()
            }
            deletePhoneButton.isEnabled = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_CONTACT_ACCESS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                activateContactButtons()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your contacts can not be edited",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        galleryViewerButton.setOnClickListener {
            val intent = Intent(applicationContext, MyGalleryViewerActivity::class.java)
            startActivity(intent)
        }

        contactsButton.setOnClickListener {
            val intent = Intent(applicationContext, MyContactsActivity::class.java)
            startActivity(intent)
        }

        openDBActivityButton.setOnClickListener {
            val intent = Intent(applicationContext, MyDatabaseClientActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        activateContactButtons()
    }

    companion object {
        internal const val PERMISSIONS_REQUEST_CONTACT_ACCESS = 1
    }
}
